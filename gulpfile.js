const gulp = require("gulp");
const browserSync = require("browser-sync");
const sass = require("gulp-sass")(require("sass"));
const uglify = require("gulp-uglify");
const concat = require("gulp-concat");
const rename = require("gulp-rename");
const del = require("del");
const prefixer = require("gulp-autoprefixer");
const fileinclude = require("gulp-file-include");

gulp.task("clean", async () => {
  del.sync("dist");
});

gulp.task("html", () => {
  return gulp.src(["src/*.html"]).pipe(browserSync.stream());
});

gulp.task("scss", () => {
  return gulp
    .src("src/scss/**/*.scss")
    .pipe(sass({ outputStyle: "compressed" }))
    .pipe(
      prefixer({
        overrideBrowserslist: ["last 12 versions"],
        browsers: [
          "> 1%",
          "last 2 versions",
          "firefox >= 15",
          "opera >= 12",
          "mozila >= 15",
          "IE 8",
          "IE 9",
          "IE 10",
          "IE 11",
          "Android >= 4",
          "Chrome >= 15",
          "iOS >= 4",
          "Safari >= 5",
        ],
      })
    )
    .pipe(rename({ suffix: ".min" }))
    .pipe(gulp.dest("src/css"))
    .pipe(browserSync.stream());
});

gulp.task("css", () => {
  return gulp
    .src([
      "node_modules/normalize.css/normalize.css",
      "node_modules/slick-carousel/slick/slick.css",
    ])
    .pipe(concat("_libs.scss"))
    .pipe(gulp.dest("src/scss"))
    .pipe(browserSync.stream());
});

gulp.task("script", () => {
  return gulp.src("src/js/*.js").pipe(browserSync.stream());
});

gulp.task("js", () => {
  return gulp
    .src(["node_modules/slick-carousel/slick/slick.js"])
    .pipe(concat("libs.min.js"))
    .pipe(uglify())
    .pipe(gulp.dest("src/js"))
    .pipe(browserSync.stream());
});

gulp.task("browser-sync", () => {
  browserSync.init({
    server: {
      baseDir: "src/",
    },
  });
});

gulp.task("export", () => {
  let buildHtml = gulp.src("src/**/*.html").pipe(gulp.dest("dist"));

  let BuildCss = gulp.src("src/css/**/*.css").pipe(gulp.dest("dist/css"));

  let BuildJs = gulp.src("src/js/**/*.js").pipe(gulp.dest("dist/js"));

  let BuildFonts = gulp.src("src/fonts/**/*.*").pipe(gulp.dest("dist/fonts"));

  let BuildImg = gulp.src("src/images/**/*.*").pipe(gulp.dest("dist/images"));
});

gulp.task("watch", () => {
  gulp.watch("src/scss/**/*.scss", gulp.parallel("scss"));
  gulp.watch("src/*.html", gulp.parallel("html"));
  gulp.watch("src/js/*.js", gulp.parallel("script"));
});

gulp.task("build", gulp.series("clean", "export"));

gulp.task(
  "default",
  gulp.parallel("css", "scss", "js", "browser-sync", "watch")
);
