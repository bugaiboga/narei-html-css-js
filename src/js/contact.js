// regex layout function validate
function regexValidate(regex, text) {
  let regexValidate = regex ? regex : false;

  if (!regexValidate) return false;

  if (!text) return false;

  if (!regexValidate.test(text)) return false;

  if (regexValidate.test(text)) return true;

  return false;
}

// validate first name & last name regex
function validateName(text) {
  let regex = /^([a-zA-Z]){3,16}$/;

  return regexValidate(regex, text);
}

// validate first name & last name regex
function validatePhone(text) {
  let regex = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;

  return regexValidate(regex, text);
}

// validate first name & last name regex
function validateEmail(text) {
  let regex = /^[^@]+@\w+(\.\w+)+\w$/;

  return regexValidate(regex, text);
}

// validate first name & last name regex
function validateDomain(text) {
  let regex = /^[a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}/;

  return regexValidate(regex, text);
}

// toggle visible menu
function toggleBurgerMenu() {
  let btnOpen = document.getElementById("menu-burger");
  btnOpen.onclick = () => {
    document.querySelector(".header-menu").style.display = "block";
  };

  let btnClose = document.querySelector(".menu-header__burger-close");
  btnClose.onclick = () => {
    document.querySelector(".header-menu").style.display = "none";
  };
}

$(function () {
  toggleBurgerMenu();

  let contactForm = document.getElementById("contact-form");

  contactForm.onsubmit = (e) => {
    e.preventDefault();
    let data = new FormData(document.getElementById("contact-form"));

    // validate first name & last name start
    let firstName = document.getElementById("contact__first-name");
    let lastName = document.getElementById("contact__last-name");

    if (!validateName(firstName.value)) {
      firstName.classList.add("error-input");
    } else {
      firstName.classList.remove("error-input");
    }
    if (!validateName(lastName.value)) {
      lastName.classList.add("error-input");
    } else {
      lastName.classList.remove("error-input");
    }
    // validate first name & last name end

    // select radiu button start
    let radioBtn = document.querySelector(
      ".contact-form__checkbox input[type='radio']:checked"
    );
    let realtorInput = document.getElementById("contact-realtor");

    if (radioBtn.value === "other") {
      realtorInput.setAttribute("required", "");
    } else {
      realtorInput.removeAttribute("required");
    }
    // select radiuo button end

    // phone number validate start
    let phoneNumber = document.getElementById("contact__phone-number");

    if (!validatePhone(phoneNumber.value)) {
      phoneNumber.classList.add("error-input");
    } else {
      phoneNumber.classList.remove("error-input");
    }
    // phone number validate end

    // email validate start
    let email = document.getElementById("contact__email");

    if (!validateEmail(email.value)) {
      email.classList.add("error-input");
    } else {
      email.classList.remove("error-input");
    }
    // email validate end

    // domain validate start
    let domain = document.getElementById("contact__domain");

    if (!validateDomain(domain.value)) {
      domain.classList.add("error-input");
    } else {
      domain.classList.remove("error-input");
    }
    // domain validate end

    // comment validate start
    let comment = document.getElementById("contact__comment");

    if (comment.value.length > 10) {
      comment.classList.remove("error-input");
    } else {
      comment.classList.add("error-input");
    }
    // comment validate end

    // send data
    if (
      validateName(firstName.value) &&
      validateName(lastName.value) &&
      validatePhone(phoneNumber.value) &&
      validateEmail(email.value) &&
      validateDomain(domain.value) &&
      comment.value.length > 10
    ) {
      firstName.value = "";
      lastName.value = "";
      phoneNumber.value = "";
      email.value = "";
      domain.value = "";
      comment.value = "";

      console.log("all true");
    }
  };
});
