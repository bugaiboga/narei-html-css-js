$(function () {
  let main = document.querySelector(".main");
  let sliderItems = document.querySelectorAll(".main-slider__item");

  sliderItems.forEach((item) => (item.style.height = main.clientHeight + "px"));

  $(".main-slider__content").slick({
    dots: true,
    arrows: false,
  });

  $(".description-content__slider").slick({
    dots: true,
    arrows: false,
    slidesToShow: 3,
    slidesToScroll: 1,

    responsive: [
      {
        breakpoint: 960,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  });

  let btnNavbarOpen = document.querySelector(".navbar-header__burger");
  btnNavbarOpen.onclick = () => {
    let navbarHeader = document.querySelector(".navbar-header");
    navbarHeader.style.display = "block";
  };

  let btnCloseHeaderNavbar = document.querySelector(
    ".navbar-header__burger-close"
  );
  btnCloseHeaderNavbar.onclick = () => {
    let navbarHeader = document.querySelector(".navbar-header");
    navbarHeader.style.display = "none";
  };

  document.getElementById("#btn").onclick = () => {
    alert("btn");
  };
});
